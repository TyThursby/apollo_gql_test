/**
 * Needs to pull in DataSource from Devices table
 * Subscription mutation for wiwi_online: @Boolean changes from Stream with
 */
const { ApolloServer, gql, PubSub } = require('apollo-server');
const pubsub = new PubSub();

const TEST_ADDED = 'TEST_ADDED';


// A schema is a collection of type definitions ("typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.
  # This "Test" type defines the queryable fields for data source.
  type Subscription {
    newTest: Test
  }
  type Test {
    description: String!
    dataValue: Int!
  }
  type Mutation {
    addTest(description: String, dataValue: Int): Test
  }
  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "tests" query returns an array of zero or more Tests (defined above).
  type Query {
    tests: [Test]
  }

`;

const tests = [
    {
      description: 'test 1'
    },
    {
      description: 'test 2'
    },
    {
      description: 'test 3'
    }
  ];

  // This resolver retrieves tests from the "tests" array above and sets query and mutation
const resolvers = {
  Subscription: {
    newTest: {
      // Additional event labels can be passed to asyncIterator creation
      subscribe: () => pubsub.asyncIterator([TEST_ADDED]),
    },
  },
    Query: {
      tests: () => tests,
    },
    Mutation: {
      addTest: (_, args) => {
        const newTest = {
          description: args.description,
          dataValue: args.dataValue
        };
        pubsub.publish(TEST_ADDED, {newTest})
        tests.push(newTest);
        return newTest;
      }
    },
  };

  // The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url, subscriptionsUrl }) => {
  console.log(`?  Server ready at ${url}`);
  console.log( `? Subscriptions ready at ${subscriptionsUrl}`);
});
